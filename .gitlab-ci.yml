# ======================================================================================================================
# Global
# ======================================================================================================================
stages:
  - build
  - static analysis
  - unit test
  - e2e test
  - report
  - release
  - deploy
  # needed for templates to work
  - test

variables:
  CURRENT_TAG: ${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHORT_SHA}
  APP_IMAGE: ${CI_REGISTRY_IMAGE}:${CURRENT_TAG}
  TEST_IMAGE: ${CI_REGISTRY_IMAGE}/test:${CURRENT_TAG}
  MOCK_IMAGE: thiht/smocker:0.17.1
  CODACY_VERSION: "13.5.3"
  CODECOV_VERSION: "0.1.9"
  CHART_VERSION: "^0.2.0"
  DEPENDENCY_SCANNING_DISABLED: "true"
  SAST_DISABLED: "true"
  CONTAINER_SCANNING_DISABLED: "true"
  CODE_QUALITY_DISABLED: "true"

include:
  - local: .gitlab/ci/jobs.gitlab-ci.yml
  - local: .gitlab/ci/rules.gitlab-ci.yml

# ======================================================================================================================
# Pre Stage
# ======================================================================================================================
cache-dependencies:
  extends:
    - .rules:main
    - .cache_dependencies

# ======================================================================================================================
# Build Stage
# ======================================================================================================================
build-app-image:
  extends:
    - .build_app_image
    - .rules:main

build-ci-image:
  extends:
    - .build_ci_image
    - .rules:build-ci-image

build-test-image:
  extends:
    - .build_test_image
    - .rules:main

# ======================================================================================================================
# Static analysis stage
# ======================================================================================================================
rubocop:
  extends:
    - .rubocop
    - .rules:main
  needs:
    - cache-dependencies

reek:
  extends:
    - .reek
    - .rules:main
  needs:
    - cache-dependencies

brakeman:
  extends:
    - .brakeman
    - .rules:main
  needs:
    - cache-dependencies

bundle-audit:
  extends:
    - .bundle_audit
    - .rules:main
  needs:
    - cache-dependencies

dependency-scan:
  extends:
    - .dependency_scan
    - .rules:dependency-scan
  needs:
    - cache-dependencies

container-scan:
  extends:
    - .container_scanning
    - .rules:container-scan
  needs:
    - build-app-image

# ======================================================================================================================
# Unit Test Stage
# ======================================================================================================================
rspec:
  extends:
    - .rspec
    - .rules:main
  needs:
    - cache-dependencies

# ======================================================================================================================
# E2E Test Stage
# ======================================================================================================================
standalone:
  extends:
    - .standalone
    - .rules:main:e2e
  needs:
    - build-app-image
    - build-test-image

# ======================================================================================================================
# Reporting
# ======================================================================================================================
publish-allure-reports:
  extends:
    - .allure_report
    - .rules:allure-reports
  needs:
    - rspec

# ======================================================================================================================
# Release Stage
# ======================================================================================================================
release-image:
  extends:
    - .release_image
    - .rules:release
  dependencies: []

create-gitlab-release:
  extends:
    - .gitlab_release
    - .rules:release
  dependencies: []

update-helm-chart:
  extends:
    - .update_chart
    - .rules:release
  dependencies: []

update-standalone-repo:
  extends:
    - .update_standalone
    - .rules:release
  dependencies: []

# ======================================================================================================================
# Deploy Stage
# ======================================================================================================================
deploy:
  extends:
    - .deploy
    - .rules:deploy
